import subprocess
import io
import ipaddress
import encodings.idna as punycode

# IDNA support https://docs.python.org/3/library/codecs.html#module-encodings.idna

# need some query composition. for dnssec we may need
# ancilliary queries with delv or drill.
# may behove us to semi-support all 3
# I think the best strategy is to check for delv, and use
# dig if delv isn't available.

# Needed objects:
# ecdig.Response - dns response (opt, answer, authority, aditional
#   stats)
# ecdig.Record - records returned by a response or used in an update
# ecdig.query - dns query function. look at requests
# ecdig.update - dns update function
# ecdig.Query - query object: ecdig.Query.send
# ecdig.record - utility function to generate ecdig.Record objects
# subclasses of ecdig.Record for common records
#
# ldnsx is the best interface I've found. dnspython is too complicated for just
# making queries.  but, I don't like either. it should behave more like
# requests.
#
# dnssec stuff is a little hairy, and will need more thought, beyond
# +cdflag and +dnssec-level settings
#
# I hate pydoc and docstrings and pep257

'''
#############################################################################
'''

# ecdig.record() function
# 
# Utility function to classify records into ecdig.Record objects (and
# subclasses)
# 
# Recommended to use this to generate such objects, as it absorbs functionality
# parsing record strings previously provided by ecdig.Record
def record(recordstring='', name='', ttl=0, dnsclass='IN', rrtype='', data=[]):
    # TODO lhs/rhs notation?
    localdata = []
    if len(recordstring) > 0:
        __recordlist = []
        for i in recordstring.split():
            if i != '':
                __recordlist.append(i.strip('\n$'))
        name = __recordlist[0]
        try:
            ttl = int(__recordlist[1])
        except ValueError:
            __recordlist.insert(1, 'filler')
            ttl = 0
        dnsclass = __recordlist[2]
        rrtype = __recordlist[3]
        __recordlist.pop(3)
        __recordlist.pop(2)
        __recordlist.pop(1)
        __recordlist.pop(0)
        for i in __recordlist:
            localdata.append(i)
        del __recordlist
    if type(rrtype) is int:
        rrtype = int_to_type(rrtype)
    elif rrtype.upper().startswith('TYPE'):
        pass ## TODO
    if type(dnsclass) is int:
        dnsclass = int_to_class(dnsclass)

    # ensure records with empty data can be made
    if len(localdata) == 0:
        localdata.append('')

    # Determine correct RRType, by testing them. There may be a most efficient
    # order to test in: evaluate at some future point
    # it'd be nice if I could just embed the return values in a dict...
    if rrtype.upper() == 'AAAA':
       try:
           return AAAARecord(name=name, address=localdata[0],
                   ttl=ttl, dnsclass=dnsclass)
       except:
           pass
    elif rrtype.upper() == 'A6':
       try:
           return A6Record(name=name, address=localdata[0],
                   ttl=ttl, dnsclass=dnsclass)
       except:
           pass
    elif rrtype.upper() == 'A':
       try:
           return ARecord(name=name, address=localdata[0],
                   ttl=ttl, dnsclass=dnsclass)
       except:
           pass
    elif rrtype.upper() == 'CNAME':
        try:
            return CNAMERecord(name=name, cname=localdata[0], ttl=ttl,
                    dnsclass=dnsclass)
        except:
            pass
    elif rrtype.upper() == 'NS':
        try:
            return NSRecord(name=name, ns=localdata[0], ttl=ttl,
                    dnsclass=dnsclass)
        except:
            pass
    elif rrtype.upper() == 'SOA':
        try:
            return SOARecord(name=name, mname=localdata[0], rname=localdata[1],
                    serial=int(localdata[2]), refresh=int(localdata[3]), retry=int(localdata[4]),
                    expire=int(localdata[5]), minimum=int(localdata[6]), ttl=ttl, dnsclass=dnsclass)
        except:
            pass
    elif rrtype.upper() == 'PTR':
        try:
            return PTRRecord(name=name, ptrdname=localdata[0], ttl=ttl, dnsclass=dnsclass)
        except:
            pass
    else:
        try:
            return Record(name=name, data=localdata, ttl=ttl, dnsclass=dnsclass, rrtype=rrtype.upper())
        except:
            raise ValueError

# ecdig Record object
# 
# Stores records, which are used by Query objects for basically everything.
# Only the `update` and `query` utility functions do not need Record objects,
# although `query` does return Record objects inside of the Response.
# 
# record string parsing is considered deprecated and will not be patched. Please
# consider using ecdig.record() instead.
class Record:
    def __init__(self, recordstring='', name='', ttl=0, dnsclass='IN',
            rrtype='', data=[]):
        ## TODO remove this if to the else
        if len(recordstring) > 0:
            __recordlist = []
            for i in recordstring.split():
                if i != '':
                    __recordlist.append(i.strip('\n$'))
            self.name = __recordlist[0]
            try:
                self.ttl = int(__recordlist[1])
            except ValueError:
                __recordlist.insert(1, 'filler')
                self.ttl = 0
            self.dnsclass = __recordlist[2]
            self.rrtype = __recordlist[3]
            __recordlist.pop(3)
            __recordlist.pop(2)
            __recordlist.pop(1)
            __recordlist.pop(0)
            self.data = []
            for i in __recordlist:
                self.data.append(i)
            del __recordlist
        else:
            self.name = name
            self.ttl = ttl
            self.dnsclass = dnsclass
            self.rrtype = rrtype.upper()
            self.data = data

    def __str__(self):
        data = ''
        for i in self.data:
            data += ' '+str(i)
        return str(self.name+' '+str(self.ttl)+' '+str(self.dnsclass)+' '+self.rrtype+' '+data)

    def is_idna(self):
        if self.name.isascii():
            for i in self.name.split('.'):
                if i.startswith('xn--'):
                    return True
            return False
        else:
            return True

    def punycode(self):
            encoded = []
            b = b'.'
            for i in self.name.split('.'):
                if i != '':
                    encoded.append(punycode.ToASCII(i))
            return b.join(encoded).decode('ascii')

# ecdig.AAAARecord object
# 
# Subclass of ecdig.Record, specifically stores address as ipaddress.IPv6Address.
class AAAARecord(Record):
    def __init__(self, name='', address='', ttl=0, dnsclass='IN'):
        self.name = name
        self.ttl = ttl
        self.dnsclass = dnsclass
        self.rrtype = 'AAAA'
        self.data = [address]
        if address != '':
            self.address = ipaddress.IPv6Address(address)

# ecdig.A6Record object
# 
# Since it's really the same as AAAA, I'll subclass it off of that.
class A6Record(AAAARecord):
    def __init__(self, name='', address='', ttl=0, dnsclass='IN'):
        self.name = name
        self.ttl = ttl
        self.dnsclass = dnsclass
        self.rrtype = 'A6'
        self.data = [address]
        if address != '':
            self.address = ipaddress.IPv6Address(address)

# ecdig.AAAARecord object
# 
# Subclass of ecdig.Record, specifically stores address as ipaddress.IPv4Address.
class ARecord(Record):
    def __init__(self, name='', address='', ttl=0, dnsclass='IN'):
        self.name = name
        self.ttl = ttl
        self.dnsclass = dnsclass
        self.rrtype = 'A'
        self.data = [address]
        if address != '':
            self.address = ipaddress.IPv4Address(address)

# ecdig CNAMERecord object
# 
# Subclass of ecdig.Record, stores rhs in cname variable, per RFC
class CNAMERecord(Record):
    def __init__(self, name='', cname='', ttl=0, dnsclass='IN'):
        self.name = name
        self.ttl = ttl
        self.dnsclass = dnsclass
        self.rrtype = 'CNAME'
        self.data = [cname]
        self.cname = cname
    # TODO add function for checking uniqueness of lhs of CNAME.

# ecdig PTRRecord object
# 
# Subclass of ecdig.Record. Automagically reverses addresses to names if needed.
# Uses lhs as name, rhs as ptrdname, per RFC.
class PTRRecord(Record):
    def __init__(self, name='', address='', ptrdname='', ttl=0, dnsclass='IN'):
        if address != '' and not name == '':
            #magically set the name if people provide an address
            ip = ipaddress.ipaddress(address)
            self.name = ip.reverse_pointer
        else:
            self.name = name
        self.ttl = ttl
        self.dnsclass = dnsclass
        self.rrtype = 'PTR'
        self.data = [ptrdname]
        self.ptrdname = ptrdname

class NSRecord(Record):
    def __init__(self, name='', ns='', ttl=0, dnsclass='IN'):
        self.name = name
        self.ttl = ttl
        self.dnsclass = dnsclass
        self.rrtype = 'NS'
        self.data = [ns]
        self.ns = ns

class MXRecord(Record):
    def __init__(self, name='', mx='', ttl=0, dnsclass='IN'):
        self.name = name
        self.ttl = ttl
        self.dnsclass = dnsclass
        self.rrtype = 'MX'
        self.data = [mx]
        self.mx = mx

# ecdig SOARecord oject:
#
# subclass of ecdig.Record. from RFC 1035:
#
# MNAME           The <domain-name> of the name server that was the
#                 original or primary source of data for this zone.
# 
# RNAME           A <domain-name> which specifies the mailbox of the
#                 person responsible for this zone.
# 
# SERIAL          The unsigned 32 bit version number of the original copy
#                 of the zone.  Zone transfers preserve this value.  This
#                 value wraps and should be compared using sequence space
#                 arithmetic.
# 
# REFRESH         A 32 bit time interval before the zone should be
#                 refreshed.
# 
# RETRY           A 32 bit time interval that should elapse before a
#                 failed refresh should be retried.
# 
# EXPIRE          A 32 bit time value that specifies the upper limit on
#                 the time interval that can elapse before the zone is no
#                 longer authoritative.
#
# All units of time are in seconds.
class SOARecord(Record):
    def __init__(self, name='', mname='', rname='', serial=0, refresh=0, retry=0,
            expire=0, minimum=0, ttl=0, dnsclass='IN'):
        self.name = name
        self.ttl = ttl
        self.dnsclass = dnsclass
        self.rrtype = 'SOA'
        self.data = [mname, rname, serial, refresh, retry, expire, minimum]
        self.mname = mname
        self.rname = rname # email?
        self.serial = serial
        self.refresh = refresh
        self.retry = retry
        self.expire = expire
        self.minimum = minimum
    ## TODO serial calculator?
    ## TODO rname to email?
    ## TODO sanity checking


# ecdig Response object
# 
# This has the sections of a DNS response. authority, aditional, and answer
# are lists of records. query is the query sent to the server.
#
# ideally I'd subclass iterators for a lot of these internal objects, so that
# Response.answer.first().data is useful. Right now, they're instead lists,
# which may have long-term scaling issues.
class Response:
    def __init__(self):
        self.answer = []
        self.authority = []
        self.additional = []
        self.opts = []
        self.stats = {}
        self.query = Record()

    def __str__(self):
        return str(self.query.name)

# ecdig Query object:
# 
# used to form and make queries and updates. Takes as input a list of
# ecdig.Record objects, which are used to create the update or query as needed.
# 
# This does some of the heavy-lifting for formulating the queries and using utils
# like nsupdate and dig and delv.
class Query:
    def __init__(self, method='query', records = [] ):
        self.response = []
        self.records = records
        self.method = method
        
    # Send dns query
    def send(self, nameserver='', destport=53, digflags=[]):
        # call backend function/class here that actually wraps the
        # dig/nsupdate/delv or nslookup or drill/ldns semantics
        if self.method == 'query':
            for i in self.records:
                self.response.append(_digWrap(i, nameserver=nameserver,
                    destport=destport, digflags=digflags))


# ecdig query function
# 
# Use this to make queries if you don't need much control over things, and
# only need to make one. you should at minimum set a qname and probably want
# qtype (default qtype is AAAA for obvious reasons).
# 
# flags is a list of strings, and currently be any supported dig +flag. This
# will change in a future version to more generic things.
# 
# We return exactly one Response object from Query.response, as you cannot make
# multiple queries with this function in a single call.
def query(qname='.', qtype='AAAA', qclass='IN', nameserver='', flags=[]):
    ## TODO smart handling of reverse queries?
    qrecord = Record()
    qrecord.name = qname
    if type(qtype) == int:
        qrecord.rrtype = 'TYPE'+str(qtype)
    else:
        qrecord.rrtype = qtype.upper()
    qrecord.dnsclass = qclass
    q = Query(records = [qrecord])
    q.send(nameserver=nameserver, digflags=flags)
    return q.response[0]

# ecdig _digWrap function
# 
# Internal utility function. Best not to use, as it is an unstable
# API used by Query.send() until I create a more sane class structure
# for my backends.
# 
# Returns a Response object
def _digWrap(qrecord, nameserver='', destport=53, digflags=[]):
    commandline = ['dig']
    if qrecord.is_idna:
        # always disable idn intelligence, because it may be broken!
        commandline.append('-q'+qrecord.punycode())
        digflags.append('+noidnin')
        digflags.append('+noidnout')
    else:
        commandline.append('-q'+qrecord.name)
    commandline.append('-t'+qrecord.rrtype)
    commandline.append('-c'+qrecord.dnsclass)
    if nameserver != '':
        commandline.append('@'+nameserver)
        commandline.append('-p'+str(destport))
    if len(digflags) > 0:
        for i in digflags:
            commandline.append(i)
    digrun = subprocess.run(commandline, stdout=subprocess.PIPE,\
            stderr=subprocess.PIPE, universal_newlines=True)
    output = io.StringIO(digrun.stdout)
    stats = []
    auth = []
    answer = []
    additional = []
    opt = []
    for row in output:
        # note: any subloop is followed by an *if*. because this is
        # a parsing nightmare to join n>1 lines.
        if row == '\n':
            continue
        if row.startswith('; <<>> DiG'):
            # TODO consider grabbing dig version here!
            commandline = row
            continue
        elif row.startswith(';; ->>HEADER<<-'):
            header = row.strip('^;; ->>HEADER<<-')
            header = header.strip('\n')
            row = next(output)
            header+=','+row.strip('^;; ')
            headers = header.replace(';',',').split(',')
            # clean up messy headers
            for i in headers:
                opt.append(i.strip('^ ').strip('\n$'))
            continue
        elif row.startswith(';; OPT'):
            row = next(output)
            while not row.startswith(';; '):
                opt.append(row.strip('\n'))
                row = next(output)
        if row.startswith(';; QUESTION'):
            row = next(output)
            temp = row.strip('^;')
            q = record(row.strip('^;'))
            continue
        elif row.startswith(';; ANSWER'):
            row = next(output)
            while row != '\n' and not row.startswith(';; '):
                answer.append(record(row))
                row = next(output)
        if row.startswith(';; AUTHORITY'):
            row = next(output)
            while row != '\n' and not row.startswith(';; '):
                auth.append(record(row))
                row = next(output)
        if row.startswith(';; ADDITIO'):
            row = next(output)
            while row != '\n' and not row.startswith(';; '):
                additional.append(record(row))
                row = next(output)
        if row.startswith(';; Query time:'):
            stats.append(row.strip('\n'))
        elif row.startswith(';; SERVER'):
            stats.append(row.strip('\n'))
        elif row.startswith(';; WHEN'):
            stats.append(row.strip('\n'))
        elif row.startswith(';; MSG SI'):
            stats.append(row.strip('\n'))
        elif qrecord.rrtype.lower() == 'axfr' or qrecord.rrtype.lower() == 'ixfr':
            while row != '\n' and not row.startswith(';; '):
                answer.append(record(row))
                row = next(output)
        elif row == '\n':
            # I know this seems redundant. this is a hell of my own design.
            continue
        else:
            ### Just in case
            print("This is Unexpected: ", row)
    r = Response()
    r.opts = {}
    # convert our opts into a dict that is useful.
    for i in opt:
        tmp = i.split()
        if len(tmp) > 2 or tmp[0] == 'flags:':
            r.opts.update({tmp[0].strip(':'): tmp[1:]})
        elif tmp[1].isnumeric():
            r.opts.update({tmp[0].strip(':'): int(tmp[1])})
        else:
            r.opts.update({tmp[0].strip(':'): tmp[1]})
    try:
        r.query = q
    except:
        r.query = qrecord
    r.answer = answer
    r.authority = auth
    r.additional = additional
    r.stats = {'stats': stats}
    return r

def _nsupdatewrap():
    commandline = ['nsupdate']
    commandline.append('')
    if len(nsupdateflags) > 0:
        for i in nsupdateflags:
            commandline.append(i)
    nsupdaterun = subprocess.run(commandline, stdout=subprocess.PIPE,\
            stderr=subprocess.PIPE, universal_newlines=True)
    output = io.StringIO(nsupdaterun.stdout)
    return

# TYPE fields are used in resource records.  Note that these types are a
# subset of QTYPEs. (rfc 1035)
# 
# TYPE            value and meaning
# 
# A               1 a host address
# 
# NS              2 an authoritative name server
# 
# MD              3 a mail destination (Obsolete - use MX)
# 
# MF              4 a mail forwarder (Obsolete - use MX)
# 
# CNAME           5 the canonical name for an alias
# 
# SOA             6 marks the start of a zone of authority
# 
# MB              7 a mailbox domain name (EXPERIMENTAL)
# 
# MG              8 a mail group member (EXPERIMENTAL)
# 
# MR              9 a mail rename domain name (EXPERIMENTAL)
# 
# NULL            10 a null RR (EXPERIMENTAL)
# 
# WKS             11 a well known service description
# 
# PTR             12 a domain name pointer
# 
# HINFO           13 host information
# 
# MINFO           14 mailbox or mail list information
# 
# MX              15 mail exchange
# 
# TXT             16 text strings
def int_to_type(rrtypeint):
    l = {   1:  'A',
            2:  'NS',
            3:  'MD',
            4:  'MF',
            5:  'CNAME',
            6:  'SOA',
            7:  'MB',
            8:  'MG',
            9:  'MR',
            10: 'NULL',
            11: 'WKS',
            12: 'PTR',
            13: 'HINFO',
            14: 'MINFO',
            15: 'MX',
            16: 'TXT',
            17: 'RP',
            18: 'AFSDB',
            19: "X25",
            20: "ISDN",
            21: "RT",
            22: "NSAP",
            23: "NSAP-PTR",
            24: "SIG",
            25: "KEY",
            26: "PX",
            27: "GPOS",
            28: "AAAA",
            29: "LOC",
            30: "NXT",
            31: "EID",
            32: "NIMLOC",
            33: "SRV",
            34: "ATMA",
            35: "NAPTR",
            36: "KX",
            37: "CERT",
            38: "A6",
            39: "DNAME",
            40: "SINK",
            41: "OPT",
            42: "APL",
            43: "DS",
            44: "SSHFP",
            45: "IPSECKEY",
            46: "RRSIG",
            47: "NSEC",
            48: "DNSKEY",
            49: "DHCID",
            50: "NSEC3",
            51: "NSEC3PARAM",
            52: "TLSA",
            53: "SMIMEA",
            54: "TYPE54",
            55: "HIP",
            56: "NINFO",
            57: "RKEY",
            58: "TALINK",
            59: "CDS",
            60: "CDNSKEY",
            61: "OPENPGPKEY",
            62: "CSYNC",
            63: "ZONEMD",
            # TODO special types
            252: "AXFR",
            253: "MAILB",
            254: "MAILA",
            255: "ANY"
         }
    if rrtypeint not in l:
        return 'TYPE'+str(rrtypeint)
    else:
        return l[rrtypeint]
    return

# See int_to_type()
def type_to_int(rrtype):
    l = {
            "A": 1,
            "NS": 2,
            "MD": 3,
            "MF": 4,
            "CNAME": 5,
            "SOA": 6,
            "MB": 7,
            "MG": 8,
            "MR": 9,
            "NULL": 10,
            "WKS": 11,
            "PTR": 12,
            "HINFO": 13,
            "MINFO": 14,
            "MX": 15,
            "TXT": 16,
            "RP": 17,
            "AFSDB": 18,
            "X25": 19,
            "ISDN": 20,
            "RT": 21,
            "NSAP": 22,
            "NSAP-PTR": 23,
            "SIG": 24,
            "KEY": 25,
            "PX": 26,
            "GPOS": 27,
            "AAAA": 28,
            "LOC": 29,
            "NXT": 30,
            "EID": 31,
            "NIMLOC": 32,
            "SRV": 33,
            "ATMA": 34,
            "NAPTR": 35,
            "KX": 36,
            "CERT": 37,
            "A6": 38,
            "DNAME": 39,
            "SINK": 40,
            "OPT": 41,
            "APL": 42,
            "DS": 43,
            "SSHFP": 44,
            "IPSECKEY": 45,
            "RRSIG": 46,
            "NSEC": 47,
            "DNSKEY": 48,
            "DHCID": 49,
            "NSEC3": 50,
            "NSEC3PARAM": 51,
            "TLSA": 52,
            "SMIMEA": 53,
            "TYPE54": 54,
            "HIP": 55,
            "NINFO": 56,
            "RKEY": 57,
            "TALINK": 58,
            "CDS": 59,
            "CDNSKEY": 60,
            "OPENPGPKEY": 61,
            "CSYNC": 62,
            "ZONEMD": 63,
            # TODO special types
            "AXFR": 252,
            "MAILB": 253,
            "MAILA": 254,
            "ANY": 255,
            "*": 255
    }
    if rrtype not in l and rrtype.startswith('TYPE'):
        ## TODO
        return
    else:
        return l[rrtype]
    return

def class_to_int(qclass):
    l = {
            "IN": 1,
            "CS": 2,
            "CH": 3,
            "HS": 4,
            "*" : 255
    }
    if qclass not in l:
        return 0
    else:
        return l[qclass]
    return

def int_to_class(qclassint):
    l = {
            1: "IN",
            2: "CS",
            3: "CH",
            4: "HS",
            255: "*"
    }
    if qclassint not in l:
        raise ValueError
    else:
        return l[qclassint]
    return
'''
############################################################################
'''

# Copyright 2019-2020 Eric C. Landgraf
# 
# Please see README.md for license info
