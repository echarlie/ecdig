# ecdig - echarlie's easy dns lookup library

DNS libraries for python3 suck. [LDNSX](https://github.com/colah/ldnsx) has an
okay interface, but isn't available for python3, and it's own base,
python-ldns, is python2-only. [dnspython](http://www.dnspython.org/) is
fairly complete, but has some legacy python2 cruft (where python3 ipaddress
would be awesome) and is overwhelmingly large.  I also just don't like the
interface.

My goal here was to make a minimal python3-only tool for resolving names and
sending dns updates, centered around tools that do that well---ISC's
bind-utils (specifically `dig` and later `nsupdate` and `delv`)---with an
API that doesn't suck (inspired by [Python
requests 2](https://2.python-requests.org//en/master/))

Currently, it should be able to handle any type of query (including AXFR,
perhaps IXFR). Some query types, including AAAA, A, SOA, and PTR have
specialty Record subclasses. `ecdig.record` is a generic wrapper around
`ecdig.Record` and its subclasses (e.g. `ecdig.AAAARecord`); it returns
whatever subclass is generated.

There is some special handling for IDNs/Punycode, however, by design most of
this is abstracted out of DiG; it is easy to build DiG without IDNA support,
and I don't want to detect DiG versions.


## Example:

Simple query:

```
>>> import ecdig
>>> q = ecdig.query(qname='landgr.af', qtype='AAAA')
>>> q
<ecdig.Response object at 0x7f9406451630>
>>> q.answer[0]
<ecdig.Record object at 0x7f9406487be0>
>>> print( q.answer[0].name, q.answer[0].ttl, q.answer[0].data )
landgr.af. 14400 ['2a00:1768:1001:37:d873:59d9:968f:3663']
```

Query the NS records for the root zone, and get the additional section:

```
>>> for i in ecdig.query(qtype='NS').additional:
...  print(i.name, i.data)
... 
a.root-servers.net. ['198.41.0.4']
b.root-servers.net. ['199.9.14.201']
c.root-servers.net. ['192.33.4.12']
d.root-servers.net. ['199.7.91.13']
e.root-servers.net. ['192.203.230.10']
f.root-servers.net. ['192.5.5.241']
g.root-servers.net. ['192.112.36.4']
h.root-servers.net. ['198.97.190.53']
i.root-servers.net. ['192.36.148.17']
j.root-servers.net. ['192.58.128.30']
k.root-servers.net. ['193.0.14.129']
l.root-servers.net. ['199.7.83.42']
m.root-servers.net. ['202.12.27.33']
a.root-servers.net. ['2001:503:ba3e::2:30']
b.root-servers.net. ['2001:500:200::b']
c.root-servers.net. ['2001:500:2::c']
d.root-servers.net. ['2001:500:2d::d']
e.root-servers.net. ['2001:500:a8::e']
f.root-servers.net. ['2001:500:2f::f']
g.root-servers.net. ['2001:500:12::d0d']
h.root-servers.net. ['2001:500:1::53']
i.root-servers.net. ['2001:7fe::53']
j.root-servers.net. ['2001:503:c27::2:30']
k.root-servers.net. ['2001:7fd::1']
l.root-servers.net. ['2001:500:9f::42']
m.root-servers.net. ['2001:dc3::35']
```

More complicated; perform multiple queries and get them all back in a single
`ecdig.Response`

```
>>> q = ecdig.Query()
>>> for i in [ 'A', 'AAAA', 'LOC', 'MX', 'TXT', 'SOA', 'NS' ]:
...  r = ecdig.record(name='vt.edu', rrtype=i)
...  q.records.append(r)
>>> q.send(nameserver='nomen.cns.vt.edu', digflags=['+norecurse'])
>>> for i in q.response:
...  for j in i.answer:
...   print(j.name, j.rrtype, j.data)
vt.edu. A ['198.82.215.14']
vt.edu. AAAA ['2607:b400:92:26:0:97:1e7:3947']
vt.edu. LOC ['37', '12', '15.000', 'N', '80', '24', '48.000', 'W', '616.00m', '1m', '10000m', '10m']
vt.edu. MX ['10', 'inbound.smtp.vt.edu.']
vt.edu. TXT ['"v=spf1', 'ip4:128.173.0.0/16', 'ip4:198.82.0.0/16', 'ip6:2001:468:c80::/48', 'ip6:2607:b400::/40', 'mx', 'include:spf.protection.outlook.com', 'include:_spf.google.com', 'include:spf.mail.vt.edu', '~all"']
vt.edu. TXT ['"MS=ms89795818"']
vt.edu. TXT ['"fZeSGIGyGo0lhtHPHmPr5qgf9EsfOOsrW573IJd9ZmDfb4I2J+I2WrsKc97+xLB/nE9n20tPvB28osvIdDGG0w=="']
vt.edu. TXT ['"MS=ms15905841"']
vt.edu. SOA ['jeru.cns.vt.edu.', 'hostmaster.vt.edu.', '201906111', '3600', '900', '604800', '600']
vt.edu. NS ['auth1.dns.cogentco.com.']
vt.edu. NS ['auth2.dns.cogentco.com.']
vt.edu. NS ['clature.cns.vt.edu.']
vt.edu. NS ['nomen.cns.vt.edu.']
```

## Future development

- Parsing headers and options (high priority)
- `ecdig.Response` is not great.
- non-trivial reverse DNS handling
- `nsupdate`
- ixfr
- generic "backends" api, s.t. drill or dnspython could be used.
- consts for record names?

## Requirements

You need to have `dig` installed somewhere on your system. Usually this is
available from your distribution in the `dns-utils` or `bind-utils` package.
If not, it can be obtained freely from the [Internet Systems
Consortium](https://ftp.isc.org/isc/bind9/) as part of bind9.

You also need python3. I developed against 3.5.; other versions may work but
are as yet untested.

## License

ISC

```
Copyright 2019 Eric C. Landgraf

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION
OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR IN
CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
```
